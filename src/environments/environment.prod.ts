export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyC3fy7rS5zBvEzsD5y94OfIQJL-RoBdvCY",
    authDomain: "angular-homework-97a60.firebaseapp.com",
    databaseURL: "https://angular-homework-97a60.firebaseio.com",
    projectId: "angular-homework-97a60",
    storageBucket: "angular-homework-97a60.appspot.com",
    messagingSenderId: "643833259836",
    appId: "1:643833259836:web:ba4ef3cc8d2f172144e039",
    measurementId: "G-1QTCTR931D"
  }
};
