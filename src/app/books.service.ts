import { AuthService } from './auth.service';
import { element } from 'protractor';
import { BooksComponent } from './books/books.component';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreCollectionGroup } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { Observable, observable } from 'rxjs';
import { tap, catchError, map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

    books:any= [{title:'Alice in Wonderland', author:'Lewis Carrol'},
                {title:'War and Peace', author:'Leo Tolstoy'},
                {title:'The Magic Mountain', author:'Thomas Mann'}];

    userCollection: AngularFirestoreCollection= this.db.collection('Users');
    bookCollection: AngularFirestoreCollection;


/*getBooks()
    {
      //  setInteval(function(),time)
      //  arrow function
      setInterval(()=>this.books,1000);
        
      // can be also:
      //  return this.books
    }
    */
    
    // getBooks()
    // {
    //   const booksObservable= new Observable
    //   (
    //     observer => 
    //     {
    //       setInterval(
    //         () => observer.next(this.books),2000    //observer.next() יעשה את הפעולה בתוך הסוגריים
    //       )
    //     }
    //   )
    //   return booksObservable;
    // }

    getBooks(userID): Observable<any[]> 
    {
      // return this.db.collection('Books').valueChanges({idField:'key_id'});
      this.bookCollection = this.db.collection(`Users/${userID}/Books`);
      console.log("this.booksCollection: ",this.bookCollection)
      return this.bookCollection.snapshotChanges().pipe(
                                                          map(
                                                            collection => collection.map(
                                                                                          document =>
                                                                                          {
                                                                                            const data= document.payload.doc.data();
                                                                                            data.id= document.payload.doc.id;
                                                                                            return data;
                                                                                          }
                                                                                        )
                                                            )
                                                        );
    } 

    getOneBook(userID:string,key_id:string):Observable<any>
    {
      console.log("this is the key_id: ",key_id);
      console.log(this.db.doc(`Users/${userID}/Books/${key_id}`).get());
      return this.db.doc(`Users/${userID}/Books/${key_id}`).get();
      
    }

    addBooks(userID:string, title:string, author:string)
    {
        console.log('userID in addBooks(): ', userID)
        const book={ userID:userID, title:title, author:author}
        //this.db.collection('Books').add(book);
        this.userCollection.doc(userID).collection('Books').add(book);
        
    }

    deleteBookFromCollection(userID, key_id)
    {
      console.log("In - books.ts deleteBooksFromCollection() ")
      console.log("This is the key_id: "+key_id)
      this.db.doc(`Users/${userID}/Books/${key_id}`).delete();
      console.log(key_id+" have deleted")
    }
   
    updateBook(userID:string,key_id:string,title:string,author:string)
    {
      this.db.doc(`Users/${userID}/Books/${key_id}`).update(
        {
          title:title,
          author:author
        }
      )
    }

    // addBooks()
    // {
    //   setInterval(
    //     () => this.books.push({title: 'A new book', author: 'New Writer'}), 5000
    //   )
    // }

  constructor(private db:AngularFirestore,
              private auth:AuthService, 
              ) { }




}
