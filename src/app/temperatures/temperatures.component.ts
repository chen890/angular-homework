import { TempService } from './../temp.service';
import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Weather } from '../interfaces/weather';


@Component({
  selector: 'app-temperatures',
  templateUrl: './temperatures.component.html',
  styleUrls: ['./temperatures.component.css']
})
export class TemperaturesComponent implements OnInit
{
  temp;
  city;
  tempData$:Observable<Weather>;
  image;
  errorMessage:string;
  hasError:boolean;

  public count : number=0;
 
  onClick()
  {
    this.count+=1;
  }

  // constructor(private route: ActivatedRoute) { }
  constructor(private route: ActivatedRoute, private tempService:TempService) { }

  ngOnInit()
  {
    // if (this.route.snapshot.params['temperature'] == 'E' || 
    // this.route.snapshot.params['temperature'] == 'e' || 
    // this.route.snapshot.params['temperature'] == '.') {
    //   this.temperature = null;
    // } else {
    //   this.temperature = this.route.snapshot.params['temperature'];
    // }
    // this.city = this.route.snapshot.params['city'];

    this.city=this.route.snapshot.params['city'];

    this.tempData$=this.tempService.searchWeatherData(this.city);
    this.tempData$.subscribe( data=>
                              {
                                console.log(data);
                                this.temp= data.temperature;
                                this.image= data.image;
                              },
                              error =>{                     // הערה לטעות 
                                this.hasError=true;
                                this.errorMessage=error.message
                                console.log('in the component!!!! '+error.message);
                              }
                              
                            )


  }
  
}

