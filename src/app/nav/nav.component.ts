import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { Location } from "@angular/common";
import { Router } from "@angular/router";
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent
{
  title: string;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
                                                                                              map(result => result.matches),
                                                                                              shareReplay()
                                                                                            );


    LogOut()
    {
      console.log("in nav.ts - LogOut()")
      this.authService.LogOut();
    }

    constructor(private breakpointObserver:BreakpointObserver,
                location: Location,
                router: Router, 
                public authService: AuthService,
                )
    {
      router.events.subscribe(val =>
                              {
                                if (location.path() == "/books")
                                {
                                  this.title = 'Books';
                                }
                                else if (location.path() == "/authors")
                                {
                                  this.title = 'Authors';
                                }
                                else if (location.path() == "/editauthor")
                                {
                                  this.title = 'Edit Authors';
                                } 
                                else if (location.path() == "/authorsform")
                                {
                                  this.title = 'Edit Authors Form';
                                } 
                                else if (location.path() == "/posts")
                                {
                                  this.title = 'Posts';
                                } 
                                else if (location.path() == "/bookform")
                                {
                                  this.title = 'Book Form';
                                }
                                else if (location.path() == "/postsform")
                                {
                                  this.title = 'Post Form';
                                }
                                else if (location.path() == "/bookform/:key_id")
                                {
                                  this.title = 'Book Form';
                                } 
                                else if (location.path() == "/sign-up")
                                {
                                  this.title = 'Sign Up';
                                }
                                else if (location.path() == "/login")
                                {
                                  this.title = 'Login';
                                }
                                else 
                                {
                                  this.title = "Temperatures";
                                }
                              }
                          );   
  }
}

