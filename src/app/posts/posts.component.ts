import { UsersService } from './../users.service';
import { PostsService } from './../posts.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  userId;
  name;
  Id;
  title;
  body;
  postID;
  postsData$;
  usersName$;
  postsData2$;


  constructor(private router: Router, private postsService:PostsService)
  {
  }

  ngOnInit() {
    //this.postsData$ = this.postsService.searchPostsData();
    this.postsData$ = this.postsService.getPost();

    // console.log(this.postsData$.subscribe(data => console.log("data: "+ data)));
    // this.usersName$ = this.UsersService.searchPostsUsers();
    // console.log(this.usersName$);
    

  }

  createPosts()
  {
    // console.log("in createPosts")
    this.postsService.addPostsToCollection();
    this.router.navigate(['/posts']);
    // console.log("Posts Added");
  }

  deletePosts(postID:string)
  {
    console.log("In - books.ts deleteBooks() ")
    console.log(postID)
    this.postsService.deletePostFromCollection(postID);
  }
  
}
