import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-editauthor',
  templateUrl: './editauthor.component.html',
  styleUrls: ['./editauthor.component.css']
})
export class EditauthorComponent implements OnInit {
  typesOfAuthors: object[] = 
  [
    {id:1, name:'Lewis Carrol'}, 
    {id:2, name:'Leo Tolstoy'},
    {id:3, name:'Thomas Mann'},
  ];

  constructor() { }

  ngOnInit() {
  }

}
