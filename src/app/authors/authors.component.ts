import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import {AuthorServiceService} from './../authors.service';



export interface Authors {
  id: number;
  name: string;
}

@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})
export class AuthorsComponent implements OnInit {
  id: number;
  newAuthor: string;
  
  authors$ : Observable<any>; 

  // typesOfAuthors: Authors[] = 
  // [
  //   {id:1, name:'Lewis Carrol'}, 
  //   {id:2, name:'Leo Tolstoy'},
  //   {id:3, name:'Thomas Mann'},
  // ];


  // constructor(private route:ActivatedRoute) { }

  constructor(private authors_service:AuthorServiceService) { }

  ngOnInit() {
    // this.id = +this.route.snapshot.params['id'];
    // this.newAuthor = this.route.snapshot.params['name'];
    // this.typesOfAuthors.forEach(author => {
    //   if (author.id === this.id)
    //   {
    //     author.name = this.newAuthor;
    //   }
    // });


    this.authors_service.addAuthors();
    //async pipe
    this.authors$=this.authors_service.getAuthors();

  }

}
