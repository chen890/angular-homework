
import { UsersService } from './users.service';
import { PostsService } from './posts.service';
import { TempService } from './temp.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BooksComponent } from './books/books.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { NavComponent } from './nav/nav.component';
import {MatCardModule} from '@angular/material/card';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import {MatExpansionModule} from '@angular/material/expansion';
import { TemperaturesComponent } from './temperatures/temperatures.component';
import { AuthorsComponent } from './authors/authors.component';
import { TempformComponent } from './tempform/tempform.component';
import { MatInputModule, MatSelectModule } from '@angular/material';
import { EditauthorComponent } from './editauthor/editauthor.component';
import { AuthorsFormComponent } from './authors-form/authors-form.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { HttpClientModule } from '@angular/common/http';
import { PostsComponent } from './posts/posts.component';
import { UsersComponent } from './users/users.component';
import { environment } from '../environments/environment';
import { BookFormComponent } from './book-form/book-form.component';

import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { AngularFireModule } from '@angular/fire';
import { PostsFormComponent } from './posts-form/posts-form.component';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireAuth } from '@angular/fire/auth';
import { SignUpComponent } from './sign-up/sign-up.component';
import { LoginComponent } from './login/login.component';




const appRoutes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full'},
  // { path: 'temperatures', component: TemperaturesComponent },

  { path: 'temperatures/:city', component: TemperaturesComponent },
  { path: 'books', component: BooksComponent },
  { path: 'bookform', component: BookFormComponent },
  { path: 'bookform/:key_id', component: BookFormComponent },
  { path: 'posts', component: PostsComponent },
  { path: 'postsform', component: PostsFormComponent },
  { path: 'postsform/:postID', component: PostsFormComponent},
  { path: 'authors', component: AuthorsComponent },
  { path: 'authors/:id/:name', component: AuthorsComponent },
  { path: 'tempform', component: TempformComponent },
  { path: 'sign-up', component: SignUpComponent },
  { path: 'login', component: LoginComponent },
  { path: 'editauthor', component: EditauthorComponent },
  // { path: 'authorsform', component: AuthorsFormComponent },
  { path: 'authorsform/:id/:name', component: AuthorsFormComponent },

];

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    BooksComponent,
    NavComponent,
    TemperaturesComponent,
    AuthorsComponent,
    TempformComponent,
    EditauthorComponent,
    AuthorsFormComponent,
    PostsComponent,
    UsersComponent,
    AppComponent,
    BookFormComponent,
    PostsFormComponent,
    SignUpComponent,
    LoginComponent,
    
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatExpansionModule, 
    MatCardModule,
    MatInputModule,
    MatSelectModule,
    MatFormFieldModule,
    FormsModule,
    HttpClientModule,
    AngularFireModule,
    AngularFirestoreModule,
    AngularFireStorageModule,
    AngularFireAuthModule,
    AngularFireModule.initializeApp(environment.firebase),
    RouterModule
    .forRoot(
      appRoutes,
    //   {enableTracing:true}    // this is for debugging only
    )
  ],
  providers: [TempService, PostsService, UsersService,AngularFireAuth],
  bootstrap: [AppComponent]
})
export class AppModule { }
