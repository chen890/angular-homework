import { PostsComponent } from './../posts/posts.component';
import { PostsService } from './../posts.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Observable, observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

@Component({
  selector: 'app-posts-form',
  templateUrl: './posts-form.component.html',
  styleUrls: ['./posts-form.component.css']
})
export class PostsFormComponent implements OnInit {

  post: any;
  form:any;
  postID;
  title: string;
  name:string;
  body:string;
  isEdit:boolean = false;
  buttonText:string="Add New Post";




  constructor(private route:ActivatedRoute,
              private router:Router,
              private postsService: PostsService) { }

  ngOnInit() 
  {
    this.postID=this.route.snapshot.params.postID;
    console.log("the postID: "+this.postID);

    if(this.postID !=null)
    {
      console.log("postID not null")
      this.isEdit=true;
      console.log("this isEdit: ",this.isEdit)
      this.buttonText="Update Post";
  
      this.postsService.getOnePost(this.postID).subscribe(
        post =>{
          console.log("this is the post.data title: ",post.data().title)
          console.log("this is the post.data name: ",post.data().name)
          console.log("this is the post.data body: ",post.data().body)
          this.name = post.data().name;
          this.title = post.data().title; 
          this.body= post.data().body;
        }
      )
    }
    else
    {
      console.log("adding new post")
      this.isEdit=false;
    }
  }

  createPosts()
  {
    this.postID=this.route.snapshot.params.postID;
    if(this.postID=null || this.postID==undefined)
    {
      console.log("this is the postID in createPosts(): ", this.postID)
      
      this.postsService.addNewPost(this.name,this.title, this.body);
      console.log("Post added");
      this.router.navigate(['/posts']);
    }

    else
    {
      console.log("In the createBooks() in else")
      this.postID=this.route.snapshot.params.postID;
      console.log("this.postID= ",this.postID)
      console.log("this.name= ",this.name)
      console.log("this.title= ",this.title)
      console.log("this.body= ",this.body)
      console.log("Row before going to postsService.updatePost(this.postID,this.title,this.name,this.body)")
      this.postsService.updatePost(this.postID,this.title,this.name,this.body);
      console.log("Post Updated");
      this.router.navigate(['/posts']);
    }
  
  }



}
