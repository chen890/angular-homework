import { AuthService } from './../auth.service';
import { Observable } from 'rxjs';
import { BooksService } from './../books.service';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit
{

  panelOpenState = false;
  books$ : Observable<any>; 
  books: any; 
  userID: string;

  constructor(private books_service:BooksService,
              public authService: AuthService,
              ) 
              { }


  deleteBook(key_id:string)
  {
    console.log("In - books.ts deleteBook() ")
    console.log("this is the key_id: ",key_id)
    console.log("this is the userID: ",this.userID)
    this.books_service.deleteBookFromCollection(this.userID,key_id);
  }

  ngOnInit() 
  {
    console.log("In ngOnInIt books")
    
    // this.books= this.books_service.getBooks().subscribe(
    //                                                      (books) =>this.books = books
    //                                                    )
    //this.books_service.addBooks();
    //this.books$ = this.books_service.getBooks(this.userID); 
    
    this.authService.user.subscribe(
                                      user =>
                                      {
                                        this.userID=user.uid;
                                        console.log("The userID: ",this.userID)
                                        this.books$= this.books_service.getBooks(this.userID);
                                      }
                                    )
  }




}

