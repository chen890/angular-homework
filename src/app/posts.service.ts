import { PostsComponent } from './posts/posts.component';
import { UsersComponent } from './users/users.component';
import { Users } from './interfaces/users';
import { Post } from './interfaces/posts';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class PostsService{

  private URL="https://jsonplaceholder.typicode.com/posts";
  private URL2="https://jsonplaceholder.typicode.com/users";
  private posts: Post[] = [];
  private postsCollaction: AngularFirestoreCollection<Post>;

  constructor(private http:HttpClient, private db:AngularFirestore) 
  {
    this.postsCollaction= db.collection<Post>('Posts')
  }


  searchPostsData():Observable<Post[]>
  {
    // console.log(this.http.get<Post[]>(`${this.URL}`));
    return this.http.get<Post[]>(`${this.URL}`).pipe(
      map(data =>  this.addUsersToPosts(data))
    );
  }

getAllUsers(): Observable<Users[]>{
    const users = this.http.get<Users[]>(`${this.URL2}`);
    return users;
}
addNewPost(name:string,title:string,body:string)
{
  const post={name:name,
              title:title,
              body:body
            }
  this.db.collection('Posts').add(post);
}

getPost(): Observable<any[]> {
  return this.db.collection('Posts').valueChanges({idField:'postID'});
} 
getOnePost(postID:string):Observable<any>
    {
      console.log("this is the postID: ",postID);
      return this.db.doc(`Posts/${postID}`).get();
      
    }

  deletePostFromCollection(postID)
  {
    console.log("In - Posts.service deletePostFromCollection() ")
    console.log("This is the postID: "+postID)
    this.db.doc(`Posts/${postID}`).delete();
    console.log(postID+" have deleted")
  }
    
    
    updatePost(postID,title:string,name:string, body:string)
    {
      this.db.doc(`Posts/${postID}`).update(
        {
          name:name,
          title:title,
          body:body
        })
    }

  addUsersToPosts(data: Post[]):Post[]
  {
    
    const users = this.getAllUsers();
    const postsArray =[];
    users.forEach(user => 
      {
        user.forEach(u =>
          {
            data.forEach(post =>
              {
                if(post.userId == u.id)
                {
                    postsArray.push(
                      {
                        id: post.id,
                        userId:post.userId,
                        title: post.title,
                        body: post.body,
                        userName: u.name,
                      })
                }
            })
        })
    })

    return postsArray;
}

addPostsToCollection()
    {
      // console.log("posts service method")
      const users=this.getAllUsers();
      const posts= this.http.get<Post[]>(`${this.URL}`);
      let collactionOfPosts:Observable<Post[]>;
      collactionOfPosts= this.postsCollaction.valueChanges();

      collactionOfPosts.forEach(data=>{
        console.log(data.length)
        if(data.length===0)
        {
          posts.forEach(post =>
            {
            post.forEach(singalPost =>
              {
              users.forEach(user =>
                {
               user.forEach(singalUser =>
                {
                 if(singalUser.id===singalPost.userId)
                 {
                   singalPost.userName=singalUser.name;
                   this.db.collection('Posts').add(
                     {
                     title: singalPost.title,
                     name:singalUser.name,
                     body:singalPost.body
                   })
                 }
               })
              })
            })
          })
        }
        else
        {
          console.log("This posts have added already")
        }
      })




    }


}
