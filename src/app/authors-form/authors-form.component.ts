import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-authors-form',
  templateUrl: './authors-form.component.html',
  styleUrls: ['./authors-form.component.css']
})
export class AuthorsFormComponent implements OnInit {
  // name: string;
  id: number;
  author = new FormControl();

  constructor(private route:ActivatedRoute, private router:Router) { }

  ngOnInit() {
    this.author = this.route.snapshot.params['name'];
    console.log(this.author);
    this.id = this.route.snapshot.params['id'];
  }

  onUpdate(){
    this.router.navigate(['/authors', this.id, this.author]);
  }

}
