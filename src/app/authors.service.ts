import { Injectable } from '@angular/core';
import { Observable, observable } from 'rxjs';    //adding Observable

@Injectable({
  providedIn: 'root'
})
export class AuthorServiceService {

  authors:any= 
  [
    {id:1, name:'Lewis Carrol'}, 
    {id:2, name:'Leo Tolstoy'},
    {id:3, name:'Thomas Mann'},
  ];

  getAuthors()
  {
    const authorsObservable= new Observable
    (
      observer => 
      {
        setInterval(
          () => observer.next(this.authors),5000    //observer.next() יעשה את הפעולה בתוך הסוגריים
        )
      }
    )
    return authorsObservable;
  }

  addAuthors()
    {
      setInterval(
        () => this.authors.push({name: 'New Author'}), 5000
      )
    }




  constructor() { }
}
