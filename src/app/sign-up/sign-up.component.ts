import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth, AngularFireAuthModule } from '@angular/fire/auth';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  email:string;
  password:string;


  constructor(private route:ActivatedRoute,
              private router:Router,
              private authService: AuthService) 
              { }

  ngOnInit() 
  {

  }

  createUser()
  {
    console.log('in CreateUser')
    this.authService.signUp(this.email,this.password)
    console.log('after signUp in authService')
  } 
}
