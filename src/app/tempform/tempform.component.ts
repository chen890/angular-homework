import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
 
@Component({
  selector: 'app-tempform',
  templateUrl: './tempform.component.html',
  styleUrls: ['./tempform.component.css']
})
export class TempformComponent implements OnInit {
  temperature: number;
  city: string;
  cities: object[] = [
    {id:1, name: 'London'},
    {id:2, name: 'Jerusalem'},
    {id:3, name: 'Paris'},
    {id:4, name: 'New York'},
    {id:5, name: 'Tokyo'},
    {id:6, name: 'Har Gilo'},
  ]
  constructor(private router: Router) { }

  ngOnInit() {
  }

  onSubmit()
  {
  // this.router.navigate(['/temperatures', this.temperature, this.city]);
  this.router.navigate(['/temperatures', this.city]);
  }
}
