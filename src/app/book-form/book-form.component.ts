import { Authors } from './../authors/authors.component';
import { BooksComponent } from './../books/books.component';
import { BooksService } from './../books.service';
import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable, observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../auth.service';



@Component({
  selector: 'app-book-form',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.css']
})
export class BookFormComponent implements OnInit
{

  book: any;
  form:any;
  title: string;
  author:string;
  key_id;
  isEdit:boolean = false;
  buttonText:string="Add New Book";
  userID:string;

  constructor(private route:ActivatedRoute,
              private router:Router,
              private bookService: BooksService,
              public authService: AuthService) { }
  

  createBooks()
  {
    if(this.key_id==null || this.key_id==undefined)
    {
      this.bookService.addBooks(this.userID,this.title, this.author);
      console.log("Book added");
      this.router.navigate(['/books']);
    }
    else
    {
      this.key_id=this.route.snapshot.params.key_id;
      console.log("In the createBooks() in else")
      console.log("this.key_id= ",this.key_id)
      console.log("this.key_id= ",this.title)
      console.log("this.key_id= ",this.author)
      
      this.bookService.updateBook(this.userID,this.key_id,this.title,this.author);
      this.router.navigate(['/books']);
    }
  }

  ngOnInit() 
  {
    this.key_id=this.route.snapshot.params.key_id;
    this.authService.user.subscribe(        // מייבא את מספר המשתמש
                                    user=>
                                    {
                                      this.userID = user.uid
                                      console.log(this.userID)
                                    }
                                  ); 
    console.log('userID: ', this.userID)
    console.log("the book id: "+this.key_id);
    console.log("this isEdit: ",this.isEdit)
    if(this.key_id !=null)
    {
      console.log("key_id not null")
      this.isEdit=true;
      console.log("this isEdit: ",this.isEdit)
      this.buttonText="Update Book";
  
      this.bookService.getOneBook(this.userID,this.key_id).subscribe(
                                                                      book =>
                                                                      {
                                                                        console.log("this is the book.data title: ",book.data().title)
                                                                        console.log("this is the book.data author: ",book.data().author)
                                                                        this.author = book.data().author;
                                                                        this.title = book.data().title;
                                                                      }
                                                                    );
      console.log(this.title)
      console.log(this.author)
    }
    else
    {
      console.log("adding new book")
      this.isEdit=false;
    }
  }

  
  
  
}
